FROM openjdk:8-jre
MAINTAINER Prashanth Ayyavu
LABEL version="1.0"
LABEL description="Target Interview Product Server"
EXPOSE 8080

RUN mkdir /service
COPY target/scala-2.12/*.jar /service
COPY extLibs/*.jar /service

WORKDIR /service
# RUN unzip *.jar kanela-agent*.jar && mv kanela-agent*.jar kanela-agent.jar

ENTRYPOINT exec java -jar -javaagent:kanela-agent-1.0.1.jar ProductServer*.jar