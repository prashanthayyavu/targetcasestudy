
- [TargetCaseStudy](#targetcasestudy)
  - [Requirements :](#requirements)
  - [Open Questions](#open-questions)
  - [Tech Stack](#tech-stack)
  - [Docker Image](#docker-image)
  - [Compilation](#compilation)
  - [Executing tests](#executing-tests)
  - [Running the application](#running-the-application)
  - [Elasticsearch](#elasticsearch)
    - [create the productprice index](#create-the-productprice-index)
  - [Utils](#utils)
  - [APIs](#apis)
    - [health endpoint](#health-endpoint)
    - [GET Product Info using product_id](#get-product-info-using-productid)
    - [Update price for a product using product_id](#update-price-for-a-product-using-productid)
  - [Performance Analysis](#performance-analysis)
    - [Some outliers](#some-outliers)
      - [API call to redsky.target took more time.](#api-call-to-redskytarget-took-more-time)
      - [Both redsky api client and elsclient took equal time](#both-redsky-api-client-and-elsclient-took-equal-time)
      - [Untracked blocked/waiting/idle time](#untracked-blockedwaitingidle-time)
  - [Unit tests](#unit-tests)
  - [What I didn get time to do ?](#what-i-didn-get-time-to-do)

# TargetCaseStudy

## Requirements :
- myRetail wants to make its internal data available to any number of client devices, from myRetail.com to native mobile apps
- over 200 stores across the east coast
- products API, aggregate product data from multiple sources and return it as JSON to the caller
- aggregate product data from multiple sources and return it as JSON to the caller
- Example product IDs: 15117729, 16483589, 16696652, 16752456, 15643793)  
- Example response: {"id":13860428,"name":"The Big Lebowski (Blu-ray) (Widescreen)","current_price":{"value": 13.49,"currency_code":"USD"}} 
- Performs an HTTP GET to retrieve the product name from an external API. (For this exercise the data will come from redsky.target.com, but let’s just pretend this is an internal resource hosted by myRetail)   
- Example: http://redsky.target.com/v2/pdp/tcin/13860428?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics 
- Reads pricing information from a NoSQL data store and combines it with the product id and name from the HTTP request into a single response.   
- BONUS: Accepts an HTTP PUT request at the same path (/products/{id}), containing a JSON request body similar to the GET response, and updates the product’s price in the data store.  

## Open Questions
- How many Products (Unique SKUs) we are talking about ?
- How often the prices of these Products change ?
- How many APIs calls are expected for the GET /products api ? since we say we want this data to be available to any number of client devices.
- What is the degree of synchronization, we should have around the Products info ?(say if the price is changed, how soon this should be reflected)    

## Tech Stack
- This is a `akka-http` based API server written in `Scala`.
    - I would have done this is less time using Java and Spring, but I wanted to play around with non-blocking and async features. Moreover it has been sometime since I actively used Spring framework. 
- `Kamon` is used for identifying, collect, process and publishing API level metrics.
- `Elasticsearch` is used to store the ProductPrice info.
- `sttp` is used to implement a REStful client to make calls to `redsky.target.com`
- `sbt` is the build tool.

## Docker Image
- the base image used for dockerising the app  is `openjdk:8-jre`

## Compilation
- compiling and assembling the jar can be done as `sbt clean compile assembly`

## Executing tests
- `sbt coverage test coverageReport`

## Running the application
Make sure docker daemon is running. Go to the root folder,
- `docker-compose up`
- this will bring up Single Node ElasticSearch cluster and the ProductServer app

## Elasticsearch

### create the productprice index

```
curl --location --request PUT 'localhost:9200/productprice' \
--header 'Content-Type: application/json' \
--data-raw '{
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "_doc" : {
        "properties": {
            "id":{
            	"type": "text"
            },
            "price":{
            	"type": "double"
            },
            "currency_code":{
            	"type": "text"
            }
        }
        }
    }
}'
```
 
## Utils
There are some simple Python Utils located under `./utils` folder.
- `productCrawler.py` used to identify valid productIds devices using the `redsky.target` endpoint, using the ids given in the SEM file (shared with me) as Seed.
    - Sorry. I don't have any intention to poach, rather just to collect few product Ids.
- `elsDataPopulator.py`used to seed ElasticSearch index with random price.
- `productIds` a file which has around 70 valid product Ids. 
 
## APIs

### health endpoint

`curl --request GET 'http://localhost:8080/myretail/v1/health'`

Response :

```json
{
    "builtAtMillis": "1585116389850",
    "name": "ProductServer",
    "gitCommit": "505438c39e37c9fa643f1fb4dbbad96119b2c22e",
    "scalaVersion": "2.12.8",
    "version": "1.0-SNAPSHOT",
    "sbtVersion": "1.2.0",
    "gitHeadCommitDate": "2020-03-24T22:24:24-0700",
    "builtAtString": "2020-03-25 06:06:29.850"
}
```

### GET Product Info using product_id

`curl --request GET 'http://localhost:8080/myretail/v1/products/13860433'`

Response :

```json
{
    "currency_code": "USD",
    "id": "13860433",
    "name": "Ice Age: A Mammoth Christmas Special (Blu-ray)",
    "price": 35.0
}
```

### Update price for a product using product_id

```
curl --request PUT 'http://localhost:8080/myretail/v1/products/13860433' \
--header 'Content-Type: application/json' \
--data-raw '{
	"id":"13860433",
	"price":438.7,
	"currency_code":"USD"
}
'
```

Response :

```json
{
    "currency_code": "USD",
    "id": "13860433",
    "price": 438.7
}
```

## Performance Analysis

~~~~
Using 1 GB main memory and 0.75 CPU for one instance of the app, I was able to attain a throughput of ~30 requests/second, 
with 99% of the requests were served under 192 ms, with an error rate of 0%.
~~~~

Again, this is my personal laptop. Ofcourse the numbers will be different when we deploy this is in a diff machine.
And I didn fine tune my ElasticSearch deployment.

When I tried to put heavy lod like up until 200 req/sec. I see a variety of minor bottlenecks.

### Some outliers
 
#### API call to redsky.target took more time.

![Bad Api Call](/imgs/1.png)

#### Both redsky api client and elsclient took equal time

![Bad Api Call](/imgs/2.png)

#### Untracked blocked/waiting/idle time

![Bad Api Call](/imgs/3.png)

## Unit tests
There are some basic unit tests available.

## What I didn get time to do ?
- deep analyze the async behavior of http client. Obviously there are nice tested `sttp` backends.
- try http client retry and exponential backoff.
- some more unit tests.

