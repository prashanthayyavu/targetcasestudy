lazy val akkaHttpVersion = "10.1.9"
lazy val akkaVersion = "2.5.23"

val gitCommitHash = SettingKey[String]("gitCommit")
gitCommitHash := git.gitHeadCommit.value.getOrElse("Not Set")

lazy val buildInfoSettings = Seq(
  buildInfoKeys := Seq[BuildInfoKey](name, version, gitCommitHash, scalaVersion, sbtVersion, git.gitHeadCommitDate),
  buildInfoPackage := "buildinfo",
  buildInfoOptions := Seq(BuildInfoOption.ToJson, BuildInfoOption.BuildTime)
)

lazy val commonSettings = Seq(
  organization := "com.target",
  name := "ProductServer",
  version := "1.0-SNAPSHOT",
  scalaVersion := "2.12.8",

  //Assembly
  mainClass in(Compile, run) := Some("com.target.interview.myretail.product.ProductServer"),
  mainClass in assembly := Some("com.target.interview.myretail.product.ProductServer"),
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case "module-info.class" => MergeStrategy.discard
    case x =>
      val oldStrategy = (assemblyMergeStrategy in assembly).value
      oldStrategy(x)
  }
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .enablePlugins(BuildInfoPlugin)
  .settings(buildInfoSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
      // kamon tracing
      "io.kamon" %% "kamon-bundle" % "2.0.1",
      "io.kamon" %% "kamon-apm-reporter" % "2.0.3",

      // akka
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,

      // logger
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      // logger needed to see kamon instrumentation logs
      "ch.qos.logback" % "logback-classic" % "1.2.3",

      // ES
      "com.sksamuel.elastic4s" %% "elastic4s-http" % "6.7.4",

      //http client
      // http client - sttp
      "com.softwaremill.sttp" %% "core" % "1.6.6",
      "com.softwaremill.sttp" %% "akka-http-backend" % "1.6.6",
      //json parser
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,

      //test
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
      "org.pegdown" % "pegdown" % "1.6.0" % Test,
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0-M4" % Test,
      "org.mockito" % "mockito-core" % "3.0.0" % Test
    )
  )

