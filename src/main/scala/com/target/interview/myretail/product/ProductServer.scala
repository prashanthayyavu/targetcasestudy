package com.target.interview.myretail.product

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.target.interview.myretail.product.config.BaseConfig
import com.target.interview.myretail.product.controllers.ProductRoutes
import com.target.interview.myretail.product.es.ElsClient
import com.target.interview.myretail.product.redsky.httpclient.RedskyClient
import kamon.Kamon
import org.slf4j.LoggerFactory

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

object ProductServer extends App with BaseConfig{
  private val logger = LoggerFactory.getLogger(this.getClass)
  Kamon.init()

  implicit val system: ActorSystem = ActorSystem("product-server")
  val settings = ActorMaterializerSettings(system)
  implicit val materializer: ActorMaterializer = ActorMaterializer(settings)
  implicit val executionContext: ExecutionContext = system.dispatcher

  lazy val redskyClient = new RedskyClient
  lazy val elsClient = new ElsClient

  lazy val routes: Route = new ProductRoutes(redskyClient, elsClient)(system).productRoutes

  //http-server
  val serverBinding: Future[Http.ServerBinding] = Http().bindAndHandle(routes, "0.0.0.0", 8080)

  //todo graceful shutdown when application crashes
  serverBinding.onComplete {
    case Success(bound) =>
      logger.info(s"Server online at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}/")
    case Failure(e) =>
      logger.error(s"Server could not start!")
      e.printStackTrace()
      system.terminate()
  }

  Await.result(system.whenTerminated, Duration.Inf)
}
