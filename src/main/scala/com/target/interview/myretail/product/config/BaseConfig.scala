package com.target.interview.myretail.product.config

import com.typesafe.config.ConfigFactory

trait BaseConfig {

  val config = ConfigFactory.load()
  val svcConfig = config.getConfig("productserver")

  val redskyBaseUrl = svcConfig.getString("redsky.baseUrl")
  val redskyRetry = svcConfig.getInt("redsky.retry")
  val redskyQParams = svcConfig.getString("redsky.queryParams")
}