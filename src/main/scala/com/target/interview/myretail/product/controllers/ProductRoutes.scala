package com.target.interview.myretail.product.controllers

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import buildinfo.BuildInfo
import com.target.interview.myretail.product.config.BaseConfig
import com.target.interview.myretail.product.errors.ErrorWrapper
import com.target.interview.myretail.product.es.ElsClient
import com.target.interview.myretail.product.models.{ElsProductInfo, JsonSupport, ProductInfo}
import com.target.interview.myretail.product.redsky.httpclient.RedskyClient
import kamon.Kamon
import kamon.instrumentation.futures.scala.ScalaFutureInstrumentation.trace

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}


class ProductRoutes(redskyClient: RedskyClient, elsClient: ElsClient)(implicit val system: ActorSystem) extends BaseConfig with JsonSupport{
  lazy val productRoutes: Route =
    pathPrefix("myretail") {
      pathPrefix("v1") {
        concat(
          pathPrefix("health") {
            get { ctx =>
              ctx.request.headers.foreach(header => println(header.name() + "," + header.value()))
              ctx.complete(BuildInfo.toJson)
            }
          },
          pathPrefix("products") {
            concat(
              path(Segment) {
                pid: String =>
                  concat(
                    (get | options) {
                      val pSpan = Kamon.currentSpan()
                      val resF = for {
                        title <- trace(Kamon.spanBuilder("redskyClient").asChildOf(pSpan))(redskyClient.getProductTitle(pid))
                        elsProductInfo <- trace(Kamon.spanBuilder("elsClient_GET").asChildOf(pSpan))(elsClient.getElsProductInfo(pid))
                      } yield ProductInfo(pid, title, elsProductInfo.price, elsProductInfo.currency_code)

                      onComplete(resF) {
                        case Success(productInfo) =>
                          complete(productInfo)
                        case Failure(fail) => complete(ErrorWrapper(fail))
                      }
                    },
                    put{
                      val pSpan = Kamon.currentSpan()
                      entity(as[ElsProductInfo]) { elsProductInfo =>
                        val resF = for {
                          existInfo <- trace(Kamon.spanBuilder("elsClient_GET").asChildOf(pSpan))(elsClient.getElsProductInfo(pid))
                          upd <- trace(Kamon.spanBuilder("elsClient_PUT").asChildOf(pSpan))(elsClient.updateCostInfo(elsProductInfo))
                        } yield ElsProductInfo(pid, elsProductInfo.price, elsProductInfo.currency_code)

                        onComplete(resF) {
                          case Success(elsProductInfo) =>
                            complete(elsProductInfo)
                          case Failure(fail) => complete(ErrorWrapper(fail))
                        }

                      }
                    }
                  )
              },
            )
          }
        )
      }
    }
}
