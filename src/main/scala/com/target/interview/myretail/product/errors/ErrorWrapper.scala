package com.target.interview.myretail.product.errors

import akka.http.scaladsl.model.StatusCodes
import spray.json.DefaultJsonProtocol._
import spray.json._

abstract class ErrorWrapper(val msg: String, cause: Throwable = None.orNull) extends Throwable(msg, cause)

object ErrorWrapper {

  def toJson(code: String, message: String) = {
    Map("error" -> Map("code" -> code, "message" -> message)).toJson
  }

  def apply(e: Throwable) = {
    e.printStackTrace()

    // todo publish error metrics

    val (errorCode, httpErrorCode, errorMessage) = e match {
      case _: ValidationError => (ErrorCode.ValidationError, StatusCodes.BadRequest, e.getMessage)
      case _: BadRequestError => (ErrorCode.BadRequestError, StatusCodes.BadRequest, e.getMessage)
      case _: ResourceNotFoundError => (ErrorCode.ResourceNotFoundError, StatusCodes.NotFound, e.getMessage)
      case _: InternalServerError => (ErrorCode.InternalServerError, StatusCodes.InternalServerError, "Internal Server Error")
      case _ => (ErrorCode.InternalServerError, StatusCodes.InternalServerError, "Internal Server Error")
    }

    (httpErrorCode, toJson(errorCode.toString, errorMessage))
  }
}

object ErrorCode extends Enumeration {
  type ErrorCodes = Value
  val ValidationError = Value("ValidationError")
  val InternalServerError = Value("InternalServerError")
  val ResourceNotFoundError = Value("ResourceNotFoundError")
  val BadRequestError = Value("BadRequestError")

  val RedskyApiError = Value("RedskyApiError")
  val JsonError = Value("JsonError")
  val ElsError = Value("ElsError")
}

class ValidationError(message: String, cause: Throwable = None.orNull) extends ErrorWrapper(message, cause)

class BadRequestError(message: String, cause: Throwable = None.orNull) extends ErrorWrapper(message, cause)

class ResourceNotFoundError(message: String, cause: Throwable = None.orNull) extends ErrorWrapper(message, cause)

class InternalServerError(message: String, cause: Throwable = None.orNull) extends ErrorWrapper(message, cause)

class RedskyApiError(message: String, cause: Throwable = None.orNull) extends ErrorWrapper(message, cause)

class JsonError(message: String, cause: Throwable = None.orNull) extends ErrorWrapper(message, cause)

class ElsError(message: String, cause: Throwable = None.orNull) extends ErrorWrapper(message, cause)