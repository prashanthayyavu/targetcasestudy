package com.target.interview.myretail.product.es

import java.net.UnknownHostException

import com.sksamuel.elastic4s.http.search.SearchResponse
import com.sksamuel.elastic4s.http.{ElasticClient, ElasticProperties, Response}
import com.target.interview.myretail.product.errors.{ElsError, JsonError, ResourceNotFoundError}
import spray.json._
import com.target.interview.myretail.product.models.{ElsProductInfo, JsonSupport}
import org.slf4j.LoggerFactory

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.control.NonFatal
//todo move to seperate ctx

class ElsClient extends JsonSupport {

  private val logger = LoggerFactory.getLogger(this.getClass)

  import com.sksamuel.elastic4s.http.ElasticDsl._

  val client = ElasticClient(ElasticProperties("http://elasticsearch:9200"))

  // implement a better async backend
  def getElsProductInfo(pid: String): Future[ElsProductInfo] = {
    Future {
      try {
        val response: Response[SearchResponse] = client.execute {
          search("productprice").matchQuery("id", pid)
        }.await

        val respJsonStr = response.result.hits.hits.head.sourceAsString

        try {
          val jsonAST = respJsonStr.parseJson
          val elsProductInfo: com.target.interview.myretail.product.models.ElsProductInfo = jsonAST.convertTo[com.target.interview.myretail.product.models.ElsProductInfo]
          elsProductInfo
        } catch {
          case NonFatal(e) =>
            logger.error(s"Json Error. ${e.getMessage}")
            throw new JsonError(s"Json Error. ${e.getMessage}")
        }
      } catch {
        case NonFatal(e) => e match {
          case ukh: UnknownHostException =>
            logger.error(s"ELS UnknownHostException. ${ukh.getMessage}")
            throw new ElsError(s"ELS UnknownHostException.")
          case noSuchElementException: NoSuchElementException =>
            logger.error(s"ELS noSuchElementException. ${noSuchElementException.getMessage}")
            throw new ResourceNotFoundError(s"ELS noSuchElementException.")
          case _ =>
            logger.error(s"ELS Error. ${e.getMessage}")
            throw new ElsError(s"ELS Error. ${e.getMessage}")
        }
      }
    }
  }

  def updateCostInfo(newElsPrdInfo: ElsProductInfo): Future[ElsProductInfo] = {
    Future {
      try {
        val response = client.execute {
          update(newElsPrdInfo.id).in("productprice" / "_doc").docAsUpsert(
            "id" -> newElsPrdInfo.id,
            "price" -> newElsPrdInfo.price,
            "currency_code" -> newElsPrdInfo.currency_code
          )
        }.await

        if (response.isSuccess)
          newElsPrdInfo
        else if (response.isError) {
          logger.error(s"ELS Error while update. ${response.error.toString}")
          throw new ElsError(s"ELS Error.")
        } else {
          throw new ElsError(s"Unknown ELS Error.")
        }

      } catch {
        case NonFatal(e) =>
          logger.error(s"ELS Error while update. ${e.getMessage}")
          throw new ElsError(s"ELS Error. ${e.getMessage}")
      }
    }
  }
}
