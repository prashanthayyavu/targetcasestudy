package com.target.interview.myretail.product.models

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class RedskyProductResponse(product: Product)
case class Product(item: Item)
case class Item(tcin: String, product_description: ProductDescription)
case class ProductDescription(title: String)
case class ElsProductInfo(id: String, price: Float, currency_code: String)
case class ProductInfo(id: String, name: String, price: Float, currency_code: String)


trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val productDescriptionFormat = jsonFormat1(ProductDescription)
  implicit val itemFormat = jsonFormat2(Item)
  implicit val productFormat = jsonFormat1(Product)
  implicit val productResponseFormat = jsonFormat1(RedskyProductResponse)
  implicit val elsProductInfoFormat = jsonFormat3(ElsProductInfo)
  implicit val productInfoFormat = jsonFormat4(ProductInfo)
}