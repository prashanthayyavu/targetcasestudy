package com.target.interview.myretail.product.redsky.httpclient

import com.softwaremill.sttp._
import com.target.interview.myretail.product.config.BaseConfig
import com.target.interview.myretail.product.errors.{JsonError, RedskyApiError}
import org.slf4j.LoggerFactory
import spray.json._
import com.target.interview.myretail.product.models.JsonSupport

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.control.NonFatal
//todo move to seperate ctx

import scala.concurrent.Future

class RedskyClient extends BaseConfig with JsonSupport{
  private val logger = LoggerFactory.getLogger(this.getClass)

  implicit val backend = HttpURLConnectionBackend()

  def getProductTitle(pid: String):Future[String]= {
    logger.debug(s"Getting Product info from Redsky. ID ${pid}")
    val url = redskyBaseUrl + pid + redskyQParams
    Future {
      val resp = sttp.get(uri"$url").send()
      if(resp.is200){
        val bodyStr = resp.body.right.getOrElse("")
        if(!bodyStr.isEmpty) {
          try {
            val jsonAST = bodyStr.parseJson
            val productResponse: com.target.interview.myretail.product.models.RedskyProductResponse = jsonAST.convertTo[com.target.interview.myretail.product.models.RedskyProductResponse]
            productResponse.product.item.product_description.title
          } catch {
            case NonFatal(e) =>
              logger.error(s"Json Error. ${e.getMessage}")
              throw new JsonError(s"Json Error. ${e.getMessage}")
          }
        } else {
          logger.error(s"Redsky api call returned empty response body for id = $pid. StatusCode ${resp.code.toString}")
          throw new RedskyApiError(s"Redsky api call returned empty response body for id = $pid. StatusCode ${resp.code.toString}")
        }
      } else {
        logger.error(s"Redsky api call failed for id = $pid. StatusCode ${resp.code.toString}")
        throw new RedskyApiError(s"Redsky api call failed for id = $pid. StatusCode ${resp.code.toString}")
      }
    }

    //todo async call in a diff way
    //todo retry logic

  }
}

object AkkaHttpClient extends BaseConfig{

}