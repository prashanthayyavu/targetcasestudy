package com.target.interview.myretail.product.test.controllers

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import com.target.interview.myretail.product.controllers.ProductRoutes
import com.target.interview.myretail.product.es.ElsClient
import com.target.interview.myretail.product.models.ElsProductInfo
import com.target.interview.myretail.product.redsky.httpclient.RedskyClient
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{Matchers, WordSpec}
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.duration._
import scala.concurrent.{Future}

class ProductRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest with MockitoSugar {
  val tSys = ActorSystem("Test-System")
  implicit def default(implicit system: ActorSystem) = RouteTestTimeout(new DurationInt(5).second)

  val mockRedskyClient = mock[RedskyClient]
  val mockElsClient = mock[ElsClient]

  lazy val routes = new ProductRoutes(mockRedskyClient, mockElsClient)(tSys).productRoutes

  "ProductRoutes" should {
    "return 200 for health call" in {
      val getHealthInfoReq = HttpRequest(uri = "/myretail/v1/health")

      getHealthInfoReq ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`text/plain(UTF-8)`)}
    }

    "return 200 for valid if pid" in {
      val getPrdInfoReq = HttpRequest(uri = "/myretail/v1/products/000")

      when(mockRedskyClient.getProductTitle(anyString())).thenReturn(Future.successful("SomeName"))
      when(mockElsClient.getElsProductInfo(anyString())).thenReturn(Future.successful(ElsProductInfo("000", 14, "USD")))

      getPrdInfoReq ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[String] should ===(
          """{"currency_code":"USD","id":"000","name":"SomeName","price":14.0}""".stripMargin)
      }
    }
  }
}
