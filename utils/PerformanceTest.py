import requests
import os
import time
from datetime import datetime
import json
import random

def updatePrice(currId,url,perfOp):
    price = random.randrange(0, 68, 1)
    docPayload = {
                	"id":currId,
                	"price":price,
                	"currency_code":"USD"
                }
    print(url)
    headers = {"Content-Type": "application/json"}
    resp = requests.put(url, data = json.dumps(docPayload), headers=headers)
    print(resp.status_code)
    perfOp.writelines('PUT '+url+' '+str(resp.status_code)+'\n')

def getProdInfo(currId,url,perfOp):
    print(url)
    resp = requests.get(url)
    print(resp.status_code)
    perfOp.writelines('GET '+url+' '+str(resp.status_code)+'\n')

if __name__ == "__main__":
    prdIdFile = open('productIds', 'r')
    perfOp = open('performance_results'+datetime.now().strftime("%m_%d_%Y_%H_%M_%S"), 'w')
    baseUrl = 'http://localhost:8080/myretail/v1/products/'
    ids = prdIdFile.readlines()

    for i in range(1000):
        nextId = ids[random.randrange(0, 68, 1)].rstrip()
        strippedId = nextId
        url = baseUrl+nextId
        if(i % 7 == 0):
            time.sleep(1)
        if(i % 6 == 0):
            time.sleep(0.7)
        if(i % 10 == 0):
            time.sleep(0.5)
            updatePrice(nextId, url,perfOp)
        else:
            getProdInfo(nextId, url,perfOp)

    perfOp.close()

