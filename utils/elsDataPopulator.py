import requests
import os
import time
from datetime import datetime
import json
import random
from elasticsearch import Elasticsearch

def addProdToEls(currId, url):
    price = random.randrange(20, 50, 3)
    docPayload = {
                	"id":currId,
                	"name":"testName",
                	"price":price,
                	"currency_code":"USD"
                }

    # Connect to the elastic cluster
    es=Elasticsearch([{'host':'localhost','port':9200}])
    print(es)
    es = es.index(index='productprice',doc_type='_doc',id=currId,body=docPayload)

if __name__ == "__main__":
    prdIdFile = open('productIds', 'r')
    baseUrl = 'http://localhost:9200/productprice/_doc/'

    ids = prdIdFile.readlines()

    for id in ids:
        time.sleep(0.5)
        currId = id.strip()
        url = baseUrl + currId
        addProdToEls(currId, baseUrl)

    prdIdFile.close()
    print("DONE")