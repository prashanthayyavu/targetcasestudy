import requests
import os
import time
from datetime import datetime

def getProduct(id, url, prdIdFile):
    print(url)
    resp = requests.head(url)
    print(resp.status_code)

    if(resp.status_code == 200):
        prdIdFile.writelines(id+'\n')

if __name__ == "__main__":
    prdIdFile = open('productIds'+datetime.now().strftime("%m_%d_%Y_%H_%M_%S"), 'w')
    seedIds = [15117729, 16483589, 16696652, 16752456, 15643793, 13860428]
    url = 'https://redsky.target.com/v2/pdp/tcin/'

    for i in seedIds:
        for j in range(i-50, i+50):
            time.sleep(0.5)
            getProduct(str(j), url+str(j),prdIdFile)
    prdIdFile.close()

    print("DONE")
